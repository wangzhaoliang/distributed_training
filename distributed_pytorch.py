#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 16 17:11:33 2018

@author: wangzhaoliang
"""
from __future__ import print_function
import argparse
import os
import shutil
import time


import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.distributed as dist
#import torch.optim
import torch.utils.data
import torch.utils.data.distributed
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import torchvision.models as models

import argparse

import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms

data_path = "/userhome/04_wangzhaoliang/data/"

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 20, 5, 1)
        self.conv2 = nn.Conv2d(20, 50, 5, 1)
        self.fc1 = nn.Linear(4*4*50, 500)
        self.fc2 = nn.Linear(500, 10)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.max_pool2d(x, 2, 2)
        x = F.relu(self.conv2(x))
        x = F.max_pool2d(x, 2, 2)
        x = x.view(-1, 4*4*50)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return F.log_softmax(x, dim=1)

model_names = sorted(name for name in models.__dict__
    if name.islower() and not name.startswith("__")
    and callable(models.__dict__[name]))

parser = argparse.ArgumentParser(description='PyTorch ImageNet Training')
parser.add_argument("--local_rank", type=int)
#parser.add_argument('data', metavar='DIR',
#                    help='path to dataset')
parser.add_argument('--arch', '-a', metavar='ARCH', default='resnet50',
                    choices=model_names,
                    help='model architecture: ' +
                        ' | '.join(model_names) +
                        ' (default: resnet18)')
parser.add_argument('-j', '--workers', default=4, type=int, metavar='N',
                    help='number of data loading workers (default: 4)')
parser.add_argument('--epochs', default=3, type=int, metavar='N',
                    help='number of total epochs to run')
parser.add_argument('--log-interval', type=int, default=10, metavar='N',
                        help='how many batches to wait before logging training status')
parser.add_argument('--start-epoch', default=0, type=int, metavar='N',
                    help='manual epoch number (useful on restarts)')
parser.add_argument('-b', '--batch-size', default=128, type=int,
                    metavar='N', help='mini-batch size (default: 256)')
parser.add_argument('--test-batch-size', type=int, default=1000, metavar='N',
                        help='input batch size for testing (default: 1000)')
parser.add_argument('--lr', '--learning-rate', default=0.01, type=float,
                    metavar='LR', help='initial learning rate')
parser.add_argument('--momentum', default=0.9, type=float, metavar='M',
                    help='momentum')
parser.add_argument('--weight-decay', '--wd', default=1e-4, type=float,
                    metavar='W', help='weight decay (default: 1e-4)')
parser.add_argument('--print-freq', '-p', default=10, type=int,
                    metavar='N', help='print frequency (default: 10)')
parser.add_argument('--resume', default='', type=str, metavar='PATH',
                    help='path to latest checkpoint (default: none)')
parser.add_argument('-e', '--evaluate', dest='evaluate', action='store_true',
                    help='evaluate model on validation set')
parser.add_argument('--pretrained', dest='pretrained', action='store_true',
                    help='use pre-trained model')
parser.add_argument('--world_size', default=2, type=int,
                    help='number of distributed processes')
#parser.add_argument('--dist_url', default='tcp://127.0.0.1:10001', type=str,
 #                   help='url used to set up distributed training')

parser.add_argument('--dist_backend', default='nccl', type=str,
                    help='distributed backend')

#parser.add_argument('--dist_backend', default='gloo', type=str,
 #                   help='distributed backend')

parser.add_argument('--dist_rank', default=0, type=int,
                    help='rank of distributed processes')
parser.add_argument('--ip_interface', default='ib0', type=str,
                    help='which IP interface to use for communication')



def main():
    global args, best_prec1
    best_prec1 = 0

    args = parser.parse_args()
    torch.cuda.set_device(args.local_rank)

    args.distributed = args.world_size > 1
    ###添加args.distributed = False
    #args.distributed = False
    print(args.world_size)
    # os.environ['NCCL_SOCKET_IFNAME'] = args.ip_interface

    if args.distributed:
        print('entern init')
        print('nodes init')
        dist.init_process_group(backend=args.dist_backend, init_method='env://')
        #dist.init_process_group(backend=args.dist_backend, init_method=args.dist_url,
         #                       world_size=args.world_size,rank=args.dist_rank)
        print('after init')
    print ("Starts!")

#    device = torch.device("cuda" if use_cuda else "cpu")


#    model = Net().to(device)
#    optimizer = optim.SGD(model.parameters(), lr=args.lr, momentum=args.momentum)
#
#    model = Net().to(device)

    # create model
    if args.pretrained:
        print("=> using pre-trained model '{}'".format(args.arch))
        model = models.__dict__[args.arch](pretrained=True)
    else:
#        print("=> creating model '{}'".format(args.arch))
#        model = models.__dict__[args.arch]()
#        model = Net().to(device)
        model = Net()


    if not args.distributed:
        if args.arch.startswith('alexnet') or args.arch.startswith('vgg'):
            model.features = torch.nn.DataParallel(model.features)
            model.cuda()
        else:
            model = torch.nn.DataParallel(model).cuda()

    else:
        model.cuda()
	#print('before new Dataparallel()')
        # parallel the model
        model = torch.nn.parallel.DistributedDataParallel(model)
	#print('before old Dataparallel()')
        #model  = torch.nn.DataParallel(model) # tcp nccl 2 nodes sucess
        #print('after old Dataparallel()')
        print('after new Dataparallel()')

    # define loss function (criterion) and optimizer
#    criterion = nn.CrossEntropyLoss().cuda()

    optimizer = torch.optim.SGD(model.parameters(), args.lr,
                                momentum=args.momentum,
                                weight_decay=args.weight_decay)

    # optionally resume from a checkpoint
    if args.resume:
        if os.path.isfile(args.resume):
            print("=> loading checkpoint '{}'".format(args.resume))
            checkpoint = torch.load(args.resume)
            args.start_epoch = checkpoint['epoch']
            best_prec1 = checkpoint['best_prec1']
            model.load_state_dict(checkpoint['state_dict'])
            optimizer.load_state_dict(checkpoint['optimizer'])
            print("=> loaded checkpoint '{}' (epoch {})"
                  .format(args.resume, checkpoint['epoch']))
        else:
            print("=> no checkpoint found at '{}'".format(args.resume))

    cudnn.benchmark = True
    use_cuda = True
    kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}

    # Data loading code
    print ("Loading...")

    train_dataset = datasets.MNIST(data_path, train=True, download=True,
                       transform=transforms.Compose([
                           transforms.ToTensor(),
                           transforms.Normalize((0.1307,), (0.3081,))
                       ]))

    if args.distributed:
        train_sampler = torch.utils.data.distributed.DistributedSampler(train_dataset)
    else:
        train_sampler = None

    train_loader = torch.utils.data.DataLoader(train_dataset,
        batch_size=args.batch_size, shuffle=True, **kwargs)
    val_loader = torch.utils.data.DataLoader(
        datasets.MNIST(data_path, train=False, transform=transforms.Compose([
                           transforms.ToTensor(),
                           transforms.Normalize((0.1307,), (0.3081,))
                       ])),
        batch_size=args.test_batch_size, shuffle=True, **kwargs)

    if args.evaluate:
        validate(val_loader, model, criterion)
        return

    for epoch in range(args.start_epoch, args.epochs):
        if args.distributed:
            train_sampler.set_epoch(epoch)
        adjust_learning_rate(optimizer, epoch)

        # train for one epoch
#        train(train_loader, model, criterion, optimizer, epoch)
        #divice !!
        train(args, model, train_loader, optimizer, epoch)

        # evaluate on validation set
#        prec1 = validate(val_loader, model, criterion)

        #divice !!
        prec1 = validate(args, model, val_loader)

        # remember best prec@1 and save checkpoint
        is_best = prec1 > best_prec1
        best_prec1 = max(prec1, best_prec1)
        save_checkpoint({
            'epoch': epoch + 1,
            'arch': args.arch,
            'state_dict': model.state_dict(),
            'best_prec1': best_prec1,
            'optimizer' : optimizer.state_dict(),
        }, is_best)


def train(args, model, train_loader, optimizer, epoch):
    # switch to train model 
    model.train()
#    x = torch.cuda.device_count()
#    print('cuda num '+str(x))
    for batch_idx, (data, target) in enumerate(train_loader):


#        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
#        output = model(data)

        input_var,target =torch.autograd.Variable(data),target.cuda(async=True)
        target_var = torch.autograd.Variable(target)
        # compute output
        output = model(input_var)
#        loss = criterion(output, target_var)
        loss = F.nll_loss(output, target_var)
#        # measure data loading time
#        data_time.update(time.time() - end)
#
#        target = target.cuda(async=True)
#        input_var = torch.autograd.Variable(input)
#        target_var = torch.autograd.Variable(target)
#
#        # compute output
#        output = model(input_var)
#        loss = criterion(output, target_var)
#


#        loss = F.nll_loss(output, target)
        loss.backward()
        optimizer.step()
        if batch_idx % args.log_interval == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.item()))

def validate(args, model, test_loader):
    model.eval()
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in test_loader:
#            data, target = data.to(device), target.to(device)
#            output = model(data)
#            test_loss += F.nll_loss(output, target, reduction='sum').item() # sum up batch loss
            input_var,target =torch.autograd.Variable(data),target.cuda(async=True)
            target_var = torch.autograd.Variable(target)
            output = model(input_var)
#            loss = criterion(output, target_var)
            test_loss += F.nll_loss(output, target_var).item() # sum up batch loss

            pred = output.max(1, keepdim=True)[1] # get the index of the max log-probability
            correct += pred.eq(target.view_as(pred)).sum().item()

    test_loss /= len(test_loader.dataset)

    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        test_loss, correct, len(test_loader.dataset),
        100. * correct / len(test_loader.dataset)))
    return correct / len(test_loader.dataset)


def save_checkpoint(state, is_best, filename='checkpoint.pth.tar'):
    torch.save(state, filename)
    if is_best:
        shutil.copyfile(filename, 'model_best.pth.tar')


class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


def adjust_learning_rate(optimizer, epoch):
    """Sets the learning rate to the initial LR decayed by 10 every 30 epochs"""
    lr = args.lr * (0.1 ** (epoch // 30))
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr


def accuracy(output, target, topk=(1,)):
    """Computes the precision@k for the specified values of k"""
    maxk = max(topk)
    batch_size = target.size(0)

    _, pred = output.topk(maxk, 1, True, True)
    pred = pred.t()
    correct = pred.eq(target.view(1, -1).expand_as(pred))

    res = []
    for k in topk:
        correct_k = correct[:k].view(-1).float().sum(0, keepdim=True)
        res.append(correct_k.mul_(100.0 / batch_size))
    return res


if __name__ == '__main__':
    main()
