该分布式训练支持的backend有 nccl、gloo

该代码在pytorch1.0的版本下可成功运行，若版本为0.41，backend为gloo时会在训练结束时报错。假设是两个节点（Node1和Node2）的分布式训练,则训练时的命令行如下。

Node 1: (假设Node 1的IP为: 172.17.40.1并且拥有free port 50500)，在节点1运行下面命令行：

python  -m torch.distributed.launch --nproc_per_node=1 --nnodes=2 --node_rank=0 --master_addr="172.17.40.1" --master_port=50500 distributed_pytorch.py

Node 2: 在节点2运行下面命令行（注意master_addr和master_port与Node1保持一致）：

python  -m torch.distributed.launch --nproc_per_node=1 --nnodes=2 --node_rank=1 --master_addr="172.17.40.1" --master_port=50500 distributed_pytorch.py

大于两个节点的分布式训练依次类推。
